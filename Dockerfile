FROM ubuntu:18.04

ENV BUILD_TOOLS "30.0.2"
ENV SDK_TOOLS_API "31"
ENV ANDROID_SDK_TOOLS="4333796"
ENV ANDROID_HOME "/opt/sdk"

# Install required dependencies
RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
    bzip2 \
    curl \
    git-core \
    html2text \
    openjdk-11-jdk \
    libc6-i386 \
    lib32stdc++6 \
    lib32gcc1 \
    lib32ncurses5 \
    lib32z1 \
    unzip \
    wget && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Download Android SDK tools
RUN wget http://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip -O /tmp/tools.zip && \
    mkdir -p ${ANDROID_HOME} && \
    unzip /tmp/tools.zip -d ${ANDROID_HOME} && \
    rm -v /tmp/tools.zip

# Install Android packages & libraries
RUN export PATH=$PATH:$ANDROID_HOME/tools/bin && \
    mkdir -p /root/.android/ && touch /root/.android/repositories.cfg && \
    yes | sdkmanager "--licenses" && \
    sdkmanager --verbose "build-tools;${BUILD_TOOLS}" "platform-tools" "platforms;android-${SDK_TOOLS_API}" \
    && sdkmanager --verbose "extras;android;m2repository" "extras;google;google_play_services" "extras;google;m2repository"

# Install gitlab notificator
RUN curl https://raw.githubusercontent.com/noknox/gitlab-slack-notificator/master/slack-notificate.sh -o /usr/local/bin/slack-notificate.sh && \
    chmod +x /usr/local/bin/slack-notificate.sh
